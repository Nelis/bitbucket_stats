import requests
import json
import dateutil.parser
from datetime import datetime, timezone


def main():
    print_pull_stats()



def get_pullrequests(repo):
    root = "https://api.bitbucket.org/2.0/repositories"
    suffix = "pullrequests"
    url = '/'.join([root, repo, suffix])
    response = requests.get(url)
    pulls = json.loads(response.content)
    return pulls['values']


def calculate_pull_stats(pull):
    pullDate = parse_date(pull['created_on'])
    print(pullDate)
    return {"title": pull['title'], "time_open": timedelta_now(pullDate)}


def print_pull_stats():
    print("Pullstats are")
    pulls = get_pullrequests("atlassian/python-bitbucket")
    for pull in pulls:
        stats = calculate_pull_stats(pull)
        print(stats['title'], stats['time_open'])


def parse_date(isodate):
    return dateutil.parser.parse(isodate)


def timedelta_now(date):
    return datetime.now(timezone.utc) - date

if __name__ == "__main__":
    main()