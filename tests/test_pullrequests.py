import vcr
import datetime
from dateutil.tz import tzutc
from freezegun import freeze_time
from bitstats import pullrequests


@vcr.use_cassette('tests/cassettes/test_get_pullrequests.yaml')
def test_get_pullrequests():
    pulls = pullrequests.get_pullrequests("atlassian/python-bitbucket")
    assert len(pulls) == 4


def test_date_parsing():
    date = pullrequests.parse_date('2017-11-16T17:41:22.893326+00:00')

    assert date == datetime.datetime(2017, 11, 16, 17, 41, 22,893326,tzutc())


@freeze_time("2018-02-07 12:00:00")
def test_timedelta_now():
    delta = pullrequests.timedelta_now(datetime.datetime(2018, 2, 7, 11, tzinfo=tzutc()))
    assert delta == datetime.timedelta(hours=1)

@freeze_time("2018-02-07 12:00:00")
def test_calculate_pull_stats():
    pullStat = pullrequests.calculate_pull_stats({"title": "This is a fancy pull request", "created_on": datetime.datetime(2018, 2, 7, 11, tzinfo=tzutc())})
    assert pullStat == {"title":"This is a fancy pull request", "time_open": datetime.timedelta(hours=1)}, "The pullstats seem to be incorrect"

