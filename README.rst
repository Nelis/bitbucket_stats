Install environment

.. code-block:: shell

    pipenv --three
    pipenv install vcrpy
    pipenv install nose
    pipenv install python-dateutil
    pipenv install freezegun


Running tests

.. code-block:: shell
    pipenv run nosetests